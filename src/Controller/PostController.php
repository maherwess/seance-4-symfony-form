<?php

namespace App\Controller;

use App\Entity\Ecole;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("first" , name="first")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/PostController.php',
        ]);
    }

    /**
     * @Route("/hello/{nom?}/{addresse}", name="helloName")
     */
    public function helloAction($nom,$addresse){

        $persone = [
            "name" => "Ahmed",
            "fullname" => "Aidi",
            "age" => 30,
        ];

        $form = $this->createFormBuilder()
            ->add('ecole')
            ->getForm();

        $em = $this->getDoctrine()->getManager();

        $ecole = new Ecole();

        $ecole->setNom($nom);
        $ecole->setAddresse($addresse);

        $em->persist($ecole);
        $em->flush();


        return $this->render('hello.html.twig', [
            'nomExpl' => $nom,
            "persone" => $persone,
            'user_form' => $form->createView(),
            'ecole' => $ecole
        ]);

    }




}
