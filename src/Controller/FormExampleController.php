<?php

namespace App\Controller;

use App\Entity\Ecole;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\EcoleType;

class FormExampleController extends AbstractController
{
    /**
     * @Route("/formexemple", name="form_example")
     */
    public function index(Request $request)
    {
        $ecole = new Ecole();

        $form = $this->createForm(EcoleType::class, $ecole, [
            'action' => $this->generateUrl('form_example'),
            'method' => 'GET'
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($ecole);
            $em->flush();

        }


        return $this->render('form_example/form.html.twig', [
            'controller_name' => 'FormExampleController',
            'ecole_form' => $form->createView(),
        ]);
    }
}
